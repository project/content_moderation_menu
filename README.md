# Content Moderation Menu

This module adds to the Drupal's default core menu_link_content, the ability to have workflow (from core Drupal's Content Moderation and Workflows).
PS: this module does not add the ability to create revisions.

For a fuller description of the module, visit the [project page on Drupal.org](https://www.drupal.org/project/content_moderation_menu)

## Requirements

 * Content Moderation Menu uses the following Drupal 8 Core components:
     Content Moderation, Workflows, Menu Link Content

 * There are no special requirements outside core.

## Installation

 * Install as you would normally install a contributed Drupal module. See:
     https://drupal.org/documentation/install/modules-themes/modules-8
     for further information.

## Troubleshooting

 * To submit bug reports and feature requests use
     https://www.drupal.org/project/issues/content_moderation_menu

## Maintainers

Current maintainers:
- [Romulo Zanetti](https://www.drupal.org/u/rzanetti) 2021(1.x)-current
